# This file is part of PaperMC-Docker.
#
# PaperMC-Docker is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or any later version.
#
# PaperMC-Docker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PaperMC-Docker. If not, see <https://www.gnu.org/licenses/>.

ARG MINECRAFT_SRC="/usr/src/minecraft"
ARG MINECRAFT_VERSION="1.20.2"

FROM alpine AS Base
LABEL org.opencontainers.image.authors="ultraxime@yahoo.fr"

RUN apk --no-cache --update add \
        curl \
        bash

ARG MINECRAFT_VERSION
ARG MINECRAFT_SRC

SHELL ["/bin/bash", "-c" ]

RUN echo "Fetching PaperMC" \
    && mkdir -p "$MINECRAFT_SRC" \
    && BUILD=$(curl https://api.papermc.io/v2/projects/paper/versions/$MINECRAFT_VERSION/ | sed -e 's/[^,0-9]//g' -e 's/[0-9]*,//g') \
    && curl "https://api.papermc.io/v2/projects/paper/versions/$MINECRAFT_VERSION/builds/$BUILD/downloads/paper-$MINECRAFT_VERSION-$BUILD.jar" --output "$MINECRAFT_SRC/paper-$MINECRAFT_VERSION.jar"

FROM eclipse-temurin:21-jre-alpine
LABEL org.opencontainers.image.authors="ultraxime@yahoo.fr"

RUN apk --no-cache --update add \
        bash \
        rsync \
        tmux \
        xterm \
        udev

ARG MINECRAFT_SRC
ENV MINECRAFT_SRC=$MINECRAFT_SRC
ENV MINECRAFT_HOME="/opt/minecraft"
ENV PLUGINS_DIR="/var/lib/minecraft/plugins"
ENV WORLD_DIR="/var/lib/minecraft/worlds"

RUN addgroup -S user \
    && adduser -G user -S user -H -s /bin/bash \
    && mkdir -p $MINECRAFT_HOME $MINECRAFT_SRC $PLUGINS_DIR $WORLD_DIR \
    && chown -R user:user $MINECRAFT_HOME $MINECRAFT_SRC $PLUGINS_DIR $WORLD_DIR

USER user

SHELL ["/bin/bash", "-c" ]

RUN touch "$MINECRAFT_SRC/first_time"



ARG SCRIPT="paper"
COPY --chown=user:user --chmod=544 $SCRIPT /usr/local/bin/paper
ADD --chown=user:user --chmod=544 https://gitlab.com/api/v4/projects/52101464/packages/generic/v1.2.0/linux-x86_64-musl/server-loader /usr/local/bin/server-loader

COPY --from=Base $MINECRAFT_SRC/paper*.jar $MINECRAFT_SRC/


EXPOSE 25565

VOLUME [ $MINECRAFT_HOME ]

ENTRYPOINT ["paper"]
CMD ["run"]

HEALTHCHECK \
    --timeout=2s \
    --start-period=300s \
    CMD ["paper", "healthcheck"]
