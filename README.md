| ![gplv3-or-later](https://www.gnu.org/graphics/gplv3-or-later.png) | [![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit) |
|--------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------|

# PaperMC - High Performance Minecraft Server

## About this image

This Docker image allows you to get a PaperMC instance quickly, with minimal fuss.

This image is a forked of `ultraxime/spigot`

## Base Docker image

* eclipse-temurin:21-jre-alpine

## How to use this image

### Building the image

```bash
docker build \
    -t ultraxime/papermc \
   .
```

When no option are specified, the image will build the 1.20.2 version of paper.</br>
To change this behavior, you should specified the option `--build-arg MINECRAFT_VERSION=<version>` when building the image.</br>
It have to be noted that we are running `Java 21` some older version of paper may not compile.

### Starting an instance

```bash
    docker run \
        --name papermc-instance \
        -p 0.0.0.0:25565:25565 \
        -d \
        -it \
        -e MINECRAFT_EULA=true \
        ultraxime/papermc
```

<!---
You must set the `DEFAULT_OP` variable on startup. This should be your
Minecraft username. The container will fail to run if this is not set.
--->

When starting a PaperMC instance, you must agree to the terms stated in
Minecraft's EULA. This can be done by setting the `MINECRAFT_EULA` variable
to `true`. Without this, the server will not run.

This image exposes the standard minecraft port (25565).

It is highly preferred to start the container with `-it`. This is needed in
order to allow executing console commands via `docker exec`. This also allows
PaperMC to safely shutdown when stopping the container via `docker stop`. See
the `Scripting` section for more details.

#### Commands

The image uses an entrypoint script called `paper`, which allows you to
execute preset commands. Should you attempt to execute an unrecognized command,
it will treat it as a regular shell command.

The commands are as follows:

* `run` - This runs the PaperMC server, and is the default command used by the
  container. This command can accept additional parameters. Useful when
  creating a new container via `docker create` or `docker run`
* `permissions` - This updates the permissions of all related files and
  folders. Useful when manually editing a file.
* `console` - This executes a console command. This allows system administrators
  to perform complex tasks via scripts. This feature is off by default. See the
  `Scripting` section for more details and examples.

Here are some examples on how to use these commands:

##### run - specify a different PaperMC configuration file inside /opt/minecraft

```bash
    docker run \
        --name papermc-instance \
        -p 0.0.0.0:25565:25565 \
        -d \
        -it \
        -e MINECRAFT_EULA=true \
        ultraxime/papermc
        run --spigot-settings spigot-test.yml
```

#### Scripting

Unlike other PaperMC Docker Images, this image provides a way to execute console
commands without attaching to the docker container. It lets system
administrators perform much more complex tasks, such as managing the docker
container from another docker container (e.g. deploying using Jenkins).

For those who are used to running `docker attach` inside a `screen` or `tmux`
session for scripting, this is going to be heaven.

This feature can be enabled by pasing the `-it` parameter to `docker create` or
`docker run`, which enables STDIN and TTY. This runs the PaperMC server inside a
`tmux` session. This also enables safe shutdown mode when the container is
stopped.

Once enabled, you may now execute console commands like so:

```bash
    docker exec papermc-instance paper console say hello everybody!
```

Some warnings when using this feature:

* **Be careful when attaching to the console via `docker attach`**. You are
  attaching to a `tmux` session running on the foreground with the footer
  disabled. Do not try to detach from the `tmux` session using `CTRL-b d`,
  otherwise this will stop the container. To detach from the container, use
  `CTRL-p CTRL-q`, which is the standard escape sequence for `docker attach`.

Here is an example on how to notify players that the server will be shutdown
after 60 seconds:

```bash
    #!/bin/bash
    docker exec papermc-instance paper console say We will be shutting down the server in 60s!
    docker exec papermc-instance paper console say Stop whatever you are doing!
    sleep 60
    docker exec papermc-instance paper console say We will be back in 1 hour!
    sleep 5

    # The container will send the stop console command to the server for you, to
    # ensure that the server is shutdown safely.
    #
    # Of course you can run this manually like so:
    #
    #     docker exec papermc-instance paper console stop
    #
    # But this will restart the container if the restart policy is set to always.
    docker stop -t 60 papermc-instance
```

### Data volumes

Be careful when mounting volumes, the permissions are not updated anymore. These feature may come back in future version.

There are three data volumes declared for this image:

#### /opt/minecraft

All server-related artifacts (jars, configs)' go here.

#### /var/lib/minecraft/worlds

This contains the world data. This is a deliberate decision in order to support
building Docker images with a world template (useful for custom maps).

The recommended approach to handling world data is to use a separate data
volume container. You can create one with the following command:

```bash
    docker run --name minecraft-world -v /var/lib/minecraft/worlds eclipse-temurin:21-jre-alpine true
```

#### /var/lib/minecraft/plugins

This contains the plugins and their configuration files. this is a deliberate
decision in order to support building Docker images with custom plugins.

The recommended approach to handling plugin data is to use a separate data
volume container. You can create one with the following command:

```bash
    docker run --name minecraft-plugin -v /var/lib/minecraft/plugins eclipse-temurin:21-jre-alpine true
```

### Environment Variables

The image uses environment variables to configure the JVM settings and the
server.properties.

#### MINECRAFT_EULA

`MINECRAFT_EULA` is required when starting creating a new container. You need to
agree to Minecraft's EULA before you can start PaperMC.

<!--
#### DEFAULT_OP

`DEFAULT_OP` is required when starting creating a new container.
-->

#### MINECRAFT_OPTS

You may adjust the JVM settings via the `MINECRAFT_OPTS` variable.

#### WORLD_DIR and PLUGINS_DIR

You may adjust where the plugins and world are stored via the respective variable.

#### Environment variables for server.properties

Each entry in the `server.properties` file can be changed by passing the
appropriate variable. To make it easier to remember and configure, the variable
representation of each entry is in uppercase, and uses underscore instead
of dash.

The server port cannot be changed. This has to be remapped when starting an
instance.

For reference, here is the list of environment variables for `server.properties`
that you can set:

* GENERATOR_SETTINGS
* OP_PERMISSION_LEVEL
* ALLOW_NETHER
* LEVEL_NAME
* ENABLE_QUERY
* ALLOW_FLIGHT
* ANNOUNCE_PLAYER_ACHIEVEMENTS
* LEVEL_TYPE
* ENABLE_RCON
* FORCE_GAMEMODE
* LEVEL_SEED
* SERVER_IP
* MAX_BUILD_HEIGHT
* SPAWN_NPCS
* WHITE_LIST
* SPAWN_ANIMALS
* SNOOPER_ENABLED
* ONLINE_MODE
* RESOURCE_PACK
* PVP
* DIFFICULTY
* ENABLE_COMMAND_BLOCK
* PLAYER_IDLE_TIMEOUT
* GAMEMODE
* MAX_PLAYERS
* SPAWN_MONSTERS
* VIEW_DISTANCE
* GENERATE_STRUCTURES
* MOTD

## Extending this image

This image is meant to be extended for packaging custom maps, plugins, and
configurations as Docker images. For server owners, this is the best way to
roll out configuration changes and updates to your servers.

<!--
### ONBUILD Trigger

This Docker image contains one `ONBUILD` trigger, which copies any local files
to `/usr/src/minecraft`.

When a container is started for the first time, the contents of this folder is
copied to `MINECRAFT_HOME` via `rsync`, except for anything that starts with
`world`. It will also ensure that the `MINECRAFT_HOME/plugins` folder exists,
and it will clean out any plugin jar files to make way for new ones. This is
the simplest way to roll out updates without going inside the data volume.

### World Templates

This Docker image supports the use of world templates, which is useful for
packaging custom maps. World templates should always start with `world`, which
has been a standard Minecraft convention (e.g. world, world_nether,
world_the_end). Copy your world templates to `/usr/src/minecraft` via the
`ONBUILD` trigger. During startup, it will check if `/var/lib/minecraft` is
empty. If so, it will create a copy of the world template on this folder.
-->

### JVM Arguments

You can include them via the `MINECRAFT_OPTS` variable in your Dockerfile.

## Supported Docker versions

This image has been tested on Docker version 1.41 on amd64

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
